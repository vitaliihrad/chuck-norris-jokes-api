package vitaliihraddev.chucknorrisjokesapi.ui.jokes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import vitaliihraddev.chucknorrisjokesapi.R
import vitaliihraddev.chucknorrisjokesapi.data.domain.model.Jokes

class JokesAdapter : RecyclerView.Adapter<JokesViewHolder>() {

    private val jokesList = mutableListOf<Jokes>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokesViewHolder {
       val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_jokes, parent, false)
        return JokesViewHolder(view)
    }

    override fun onBindViewHolder(holder: JokesViewHolder, position: Int) = holder.bind(jokesList[position])

    override fun getItemCount() = jokesList.size

    fun submit(data: Collection<Jokes>) {
        jokesList.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }
}