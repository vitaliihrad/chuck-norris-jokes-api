package vitaliihraddev.chucknorrisjokesapi.data.repo

import android.content.Context
import vitaliihraddev.chucknorrisjokesapi.data.domain.model.Jokes
import vitaliihraddev.chucknorrisjokesapi.data.domain.repo.JokesRepository
import vitaliihraddev.chucknorrisjokesapi.data.network.api.ChuckNorrisApiService
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf
import vitaliihraddev.chucknorrisjokesapi.util.resultof.safeCall

class JokeRepositoryImpl(
    private val chuckNorrisApiService: ChuckNorrisApiService,
    val context: Context
) :
    JokesRepository {

    override suspend fun getCategories(): ResultOf<Collection<String>> {
        return safeCall(context) {
            chuckNorrisApiService.getCategories()
        }
    }

    override suspend fun getJoke(category: String, amount: Int): Collection<Jokes> {
        val jokes = mutableListOf<Jokes>()
        var amountReceived = 0
        while (amountReceived != amount) {
            jokes.add(chuckNorrisApiService.getJoke(category).run {
                Jokes(this.value)
            }
            )
            amountReceived++
        }
        return jokes
    }
}