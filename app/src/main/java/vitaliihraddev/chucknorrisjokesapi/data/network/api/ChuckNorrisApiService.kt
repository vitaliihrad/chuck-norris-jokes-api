package vitaliihraddev.chucknorrisjokesapi.data.network.api

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import vitaliihraddev.chucknorrisjokesapi.data.network.model.JokesResponse
import vitaliihraddev.chucknorrisjokesapi.util.connection.NoConnectionInterceptor

interface ChuckNorrisApiService {

    @GET("categories")
    suspend fun getCategories(): Collection<String>

    @GET("random?")
    suspend fun getJoke(@Query("category") category: String): JokesResponse

    companion object {
        private const val BASE_URL = "https://api.chucknorris.io/jokes/"

        fun create(context: Context): ChuckNorrisApiService {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
            val noConnectionInterceptor = NoConnectionInterceptor(context)
            val client: OkHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(noConnectionInterceptor)
                .build()
            return Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ChuckNorrisApiService::class.java)
        }
    }
}