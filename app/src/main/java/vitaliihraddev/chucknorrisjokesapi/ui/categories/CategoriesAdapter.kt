package vitaliihraddev.chucknorrisjokesapi.ui.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import vitaliihraddev.chucknorrisjokesapi.R

class CategoriesAdapter(
    private val onClick: (position: Int) -> Unit
) : RecyclerView.Adapter<CategoriesViewHolder>() {

    private val categoriesList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_categories, parent, false)
        return CategoriesViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) =
        holder.bind(categoriesList[position])

    override fun getItemCount() = categoriesList.size

    fun getItem(position: Int) = categoriesList[position]

    fun submit(data: Collection<String>) {
        categoriesList.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }
}