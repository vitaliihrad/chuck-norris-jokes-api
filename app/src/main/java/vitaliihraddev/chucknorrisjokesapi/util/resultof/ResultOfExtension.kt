package vitaliihraddev.chucknorrisjokesapi.util.resultof

import android.content.Context
import vitaliihraddev.chucknorrisjokesapi.R
import vitaliihraddev.chucknorrisjokesapi.util.connection.NoConnectionInterceptor

suspend fun <T> safeCall(
    context: Context,
    networkRequest: suspend () -> Collection<T>
): ResultOf<Collection<T>> {
    return try {
        ResultOf.Success(networkRequest())
    } catch (e: NoConnectionInterceptor.NoInternetException) {
        ResultOf.Failure(e, context.getString(R.string.check_internet_connection_error))
    } catch (e: NoConnectionInterceptor.NoConnectivityException) {
        ResultOf.Failure(e, context.getString(R.string.internet_unavailable_error))
    }
}