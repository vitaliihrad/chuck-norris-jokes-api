package vitaliihraddev.chucknorrisjokesapi.ui.jokes

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import vitaliihraddev.chucknorrisjokesapi.data.domain.model.Jokes
import vitaliihraddev.chucknorrisjokesapi.data.domain.repo.JokesRepository
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf
import vitaliihraddev.chucknorrisjokesapi.util.resultof.safeCall

typealias resultOf = ResultOf<Collection<Jokes>>

class JokesViewModel(private val jokesRepositoryImpl: JokesRepository) : ViewModel() {

    private val _jokesLiveData = MutableLiveData<resultOf>()
    val jokesLiveData: LiveData<resultOf> = _jokesLiveData

    fun getJokes(category: String, amount: Int, context: Context) {
        viewModelScope.launch {
            val result = safeCall(context) { jokesRepositoryImpl.getJoke(category, amount) }
            _jokesLiveData.postValue(result)
        }
    }
}