package vitaliihraddev.chucknorrisjokesapi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.commit
import com.onesignal.OneSignal
import vitaliihraddev.chucknorrisjokesapi.ui.categories.CategoriesFragment
import vitaliihraddev.chucknorrisjokesapi.ui.webview.WebViewFragment

class MainActivity : AppCompatActivity() {

    private val progressBar by lazy { findViewById<ProgressBar>(R.id.main_progress_bar) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(ONESIGNAL_APP_ID)
        OneSignal.setNotificationOpenedHandler {
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.putExtra(EXTRA_MESSAGE, "push")
            startActivity(intent)
        }
        if (savedInstanceState == null) {
            checkOpeningNotifications()
        }
        progressBar.isVisible = false
    }

    override fun onBackPressed() {
        val myWebView = findViewById<WebView>(R.id.my_web_view)
        if (WebViewFragment.isActive) {
            if (myWebView.canGoBack()) myWebView.goBack() else super.onBackPressed()
        } else super.onBackPressed()
    }

    private fun checkOpeningNotifications() {
        if (load()) {
            openWebViewFragment()
        } else {
            checkLaunchType(intent.getStringExtra(EXTRA_MESSAGE))
        }
    }

    private fun checkLaunchType(isPushNotification: String?) {
        if (isPushNotification == null) {
            openCategoriesFragment()
        } else {
            openWebViewFragment()
            save()
        }
    }

    private fun openCategoriesFragment() {
        supportFragmentManager.commit {
            replace(
                R.id.main_fragment_container,
                CategoriesFragment.newInstance(),
                CategoriesFragment.TAG
            )
        }
    }

    private fun openWebViewFragment() {
        supportFragmentManager.commit {
            replace(
                R.id.main_fragment_container,
                WebViewFragment.newInstance(),
                WebViewFragment.TAG
            )
        }
    }

    private fun save() {
        val sharedPreferences =
            applicationContext.getSharedPreferences(LAUNCH_TYPE, Context.MODE_PRIVATE)
                .edit()
        sharedPreferences.putBoolean(LAUNCH_TYPE, true)
            .apply()
    }

    private fun load(): Boolean {
        val sharedPreferences =
            applicationContext.getSharedPreferences(LAUNCH_TYPE, Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(
            LAUNCH_TYPE,
            false
        )
    }

    companion object {
        const val EXTRA_MESSAGE: String = "Start Activity"
        const val LAUNCH_TYPE: String = "one signal"
        const val ONESIGNAL_APP_ID = "a996ab5f-5f60-4a3d-adcf-b02995198af4"
    }
}