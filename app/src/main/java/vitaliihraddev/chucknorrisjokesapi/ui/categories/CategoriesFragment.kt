package vitaliihraddev.chucknorrisjokesapi.ui.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import vitaliihraddev.chucknorrisjokesapi.R
import vitaliihraddev.chucknorrisjokesapi.databinding.FragmentCategoriesBinding
import vitaliihraddev.chucknorrisjokesapi.ui.jokes.JokesFragment
import vitaliihraddev.chucknorrisjokesapi.util.extension.getViewModelsFactory
import vitaliihraddev.chucknorrisjokesapi.util.extension.showToast
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf

class CategoriesFragment : Fragment() {

    private lateinit var binding: FragmentCategoriesBinding
    private val categoriesViewModel by viewModels<CategoriesViewModel> {
        getViewModelsFactory()
    }

    private val onCategoriesClick: (position: Int) -> Unit = { position ->
        val selectedCategory = adapter.getItem(position)
        val bundle = bundleOf(JokesFragment.CATEGORY_KEY to selectedCategory)
        parentFragmentManager.commit {
            replace(
                R.id.main_fragment_container,
                JokesFragment::class.java,
                bundle,
                JokesFragment.TAG
            )
            addToBackStack(JokesFragment.TAG)
        }
    }

    private val adapter = CategoriesAdapter(onCategoriesClick)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (savedInstanceState == null) categoriesViewModel.getCategories()
        binding = FragmentCategoriesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObserving()
        setListeners()
        binding.categoriesRecyclerView.adapter = adapter
    }

    private fun setListeners() {
        binding.apply {
            updateButton.setOnClickListener {
                categoriesProgressBar.isVisible = true
                categoriesViewModel.getCategories()
                updateButton.isVisible = false
            }
        }
    }

    private fun setObserving() {
        categoriesViewModel.categoriesLiveData.observe(viewLifecycleOwner) { result ->
            @Suppress("UNCHECKED_CAST")
            when (result) {
                is ResultOf.Success -> adapter.submit(result.value)
                is ResultOf.Failure -> {
                    result.message?.let { showToast(it) }
                    binding.updateButton.isVisible = true
                }
            }
            binding.categoriesProgressBar.isVisible = false
        }
    }

    companion object {
        val TAG: String = CategoriesFragment::class.java.name

        fun newInstance() = CategoriesFragment()
    }
}