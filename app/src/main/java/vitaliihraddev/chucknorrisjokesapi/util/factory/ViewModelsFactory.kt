package vitaliihraddev.chucknorrisjokesapi.util.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import vitaliihraddev.chucknorrisjokesapi.data.domain.repo.JokesRepository
import vitaliihraddev.chucknorrisjokesapi.data.network.api.ChuckNorrisApiService
import vitaliihraddev.chucknorrisjokesapi.data.repo.JokeRepositoryImpl
import vitaliihraddev.chucknorrisjokesapi.ui.categories.CategoriesViewModel
import vitaliihraddev.chucknorrisjokesapi.ui.jokes.JokesViewModel

class ViewModelsFactory(context: Context) : ViewModelProvider.Factory {

    private val jokesRepositoryImpl: JokesRepository = JokeRepositoryImpl(ChuckNorrisApiService.create(context), context)

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when(modelClass){
            CategoriesViewModel::class.java -> {
                @Suppress("UNCHECKED_CAST")
                CategoriesViewModel(jokesRepositoryImpl) as T
            }
            JokesViewModel::class.java ->{
                @Suppress("UNCHECKED_CAST")
                JokesViewModel(jokesRepositoryImpl) as T
            }
            else -> {
                throw Exception("ViewModel not supported")
            }
        }
    }
}