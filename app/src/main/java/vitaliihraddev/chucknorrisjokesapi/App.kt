package vitaliihraddev.chucknorrisjokesapi

import android.app.Application
import vitaliihraddev.chucknorrisjokesapi.util.factory.ViewModelsFactory

class App : Application() {

    val viewModelsFactory: ViewModelsFactory by lazy {
        ViewModelsFactory(applicationContext)
    }
}