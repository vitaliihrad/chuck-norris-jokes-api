package vitaliihraddev.chucknorrisjokesapi.ui.categories

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import vitaliihraddev.chucknorrisjokesapi.R

class CategoriesViewHolder(
    view: View,
    onClick: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view) {

    private val categoriesTextView = itemView.findViewById<TextView>(R.id.categories_text_view)
    private val categoriesCardView = itemView.findViewById<CardView>(R.id.categories_card_view)

    init {
        categoriesCardView.setOnClickListener {
            onClick(adapterPosition)
        }
    }

    fun bind(categoriesName: String) {
        categoriesTextView.text = categoriesName
    }
}