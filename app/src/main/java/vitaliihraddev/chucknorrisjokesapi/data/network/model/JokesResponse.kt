package vitaliihraddev.chucknorrisjokesapi.data.network.model

import com.google.gson.annotations.SerializedName

data class JokesResponse(
    @SerializedName("value") val value : String
)
