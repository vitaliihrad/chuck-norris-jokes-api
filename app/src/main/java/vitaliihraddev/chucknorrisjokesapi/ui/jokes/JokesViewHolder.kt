package vitaliihraddev.chucknorrisjokesapi.ui.jokes

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import vitaliihraddev.chucknorrisjokesapi.R
import vitaliihraddev.chucknorrisjokesapi.data.domain.model.Jokes

class JokesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val jokesTextView = itemView.findViewById<TextView>(R.id.jokes_text_view)

    fun bind(joke: Jokes){
        jokesTextView.text = joke.jokeValue
    }
}