package vitaliihraddev.chucknorrisjokesapi.util.extension

import android.widget.Toast
import androidx.fragment.app.Fragment
import vitaliihraddev.chucknorrisjokesapi.App

fun Fragment.getViewModelsFactory() = (requireContext().applicationContext as App).viewModelsFactory

fun Fragment.showToast(messageId: Int) {
    Toast.makeText(
        context,
        messageId,
        Toast.LENGTH_SHORT
    ).show()
}

fun Fragment.showToast(message: String) {
    Toast.makeText(
        context,
        message,
        Toast.LENGTH_SHORT
    ).show()
}