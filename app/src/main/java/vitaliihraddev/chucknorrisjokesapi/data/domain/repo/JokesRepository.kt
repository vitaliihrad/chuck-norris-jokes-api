package vitaliihraddev.chucknorrisjokesapi.data.domain.repo

import vitaliihraddev.chucknorrisjokesapi.data.domain.model.Jokes
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf

interface JokesRepository {

    suspend fun getCategories(): ResultOf<Collection<String>>

    suspend fun getJoke(category: String, amount: Int): Collection<Jokes>
}