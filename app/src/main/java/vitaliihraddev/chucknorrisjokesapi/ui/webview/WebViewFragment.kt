package vitaliihraddev.chucknorrisjokesapi.ui.webview

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import vitaliihraddev.chucknorrisjokesapi.R
import vitaliihraddev.chucknorrisjokesapi.databinding.FragmentWebViewBinding

class WebViewFragment : Fragment() {

    private lateinit var binding: FragmentWebViewBinding
    private val myWebView by lazy { binding.myWebView }
    private val myProgressBar by lazy { binding.myProgressBar }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWebViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myWebViewSetup(savedInstanceState, getString(R.string.base_url_web_view))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        myWebView.saveState(outState)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun myWebViewSetup(savedInstanceState: Bundle?, url: String) {
        myWebView.apply {
            webViewClient = WebViewClient()
            webChromeClient = object : WebChromeClient() {

                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    super.onProgressChanged(view, newProgress)
                    isActive = true
                    if (newProgress < 100 && !myProgressBar.isVisible) {
                        myProgressBar.isVisible = true
                    }
                    if (newProgress == 100) {
                        myProgressBar.isVisible = false
                    }
                }
            }
            if (savedInstanceState == null) {
                loadUrl(url)
            } else {
                restoreState(savedInstanceState)
            }
            settings.javaScriptEnabled = true
        }
    }

    companion object {
        val TAG: String = WebViewFragment::class.java.name
        var isActive: Boolean = false
        fun newInstance() = WebViewFragment()
    }
}

