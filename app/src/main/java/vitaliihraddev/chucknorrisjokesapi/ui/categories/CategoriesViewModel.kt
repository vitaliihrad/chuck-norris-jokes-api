package vitaliihraddev.chucknorrisjokesapi.ui.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import vitaliihraddev.chucknorrisjokesapi.data.domain.repo.JokesRepository
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf

typealias resultOf = ResultOf<Collection<String>>

class CategoriesViewModel(private val jokesRepositoryImpl: JokesRepository) : ViewModel() {

    private val _categoriesLiveData = MutableLiveData<resultOf>()
    val categoriesLiveData: LiveData<resultOf> = _categoriesLiveData

    fun getCategories() {
        viewModelScope.launch(Dispatchers.IO) {
            _categoriesLiveData.postValue(jokesRepositoryImpl.getCategories())
        }
    }
}