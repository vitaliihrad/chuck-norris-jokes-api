package vitaliihraddev.chucknorrisjokesapi.ui.jokes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import vitaliihraddev.chucknorrisjokesapi.databinding.FragmentJokesBinding
import vitaliihraddev.chucknorrisjokesapi.util.extension.getViewModelsFactory
import vitaliihraddev.chucknorrisjokesapi.util.extension.showToast
import vitaliihraddev.chucknorrisjokesapi.util.resultof.ResultOf

class JokesFragment : Fragment() {

    private lateinit var binding: FragmentJokesBinding
    private val jokesViewModel by viewModels<JokesViewModel> {
        getViewModelsFactory()
    }
    private val categoriesJokes: String by lazy {
        requireNotNull(requireArguments().getString(CATEGORY_KEY))
    }
    private val adapter = JokesAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (savedInstanceState == null) jokesViewModel.getJokes(
            categoriesJokes,
            15,
            requireContext()
        )
        binding = FragmentJokesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObserving()
        setListeners()
        binding.jokesRecyclerView.adapter = adapter
    }

    private fun setListeners() {
        binding.apply {
            updateButton.setOnClickListener {
                jokesProgressBar.isVisible = true
                jokesViewModel.getJokes(categoriesJokes, 15, requireContext())
                updateButton.isVisible = false
            }
        }
    }

    private fun setObserving() {
        jokesViewModel.jokesLiveData.observe(viewLifecycleOwner) { result ->
            @Suppress("UNCHECKED_CAST")
            when (result) {
                is ResultOf.Success -> adapter.submit(result.value)
                is ResultOf.Failure -> {
                    result.message?.let { showToast(it) }
                    binding.updateButton.isVisible = true
                }
            }
            binding.jokesProgressBar.isVisible = false
        }
    }

    companion object {
        const val CATEGORY_KEY: String = "category key"
        val TAG: String = JokesFragment::class.java.name
    }
}